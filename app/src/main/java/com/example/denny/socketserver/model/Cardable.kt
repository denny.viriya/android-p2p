package com.example.denny.socketserver.model

interface Cardable {
    var title: String?
    var content: String?
    var meta: String?
}
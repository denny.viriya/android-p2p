package com.example.denny.socketserver.model

import android.net.wifi.p2p.WifiP2pDevice

class CardableWifiDevice(device: WifiP2pDevice): WifiP2pDevice(device), Cardable {
    override var title: String? = deviceName
    override var content: String? = deviceAddress
    override var meta: String? = primaryDeviceType
}
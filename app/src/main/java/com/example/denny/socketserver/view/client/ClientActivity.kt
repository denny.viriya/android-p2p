package com.example.denny.socketserver.view.client

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.wifi.p2p.WifiP2pConfig
import android.net.wifi.p2p.WifiP2pManager
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.denny.socketserver.R
import com.example.denny.socketserver.view.contract.WifiP2pContract
import com.example.denny.socketserver.model.Cardable
import com.example.denny.socketserver.view.global.MessageAdapter
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.net.InetSocketAddress
import java.net.Socket
import java.net.UnknownHostException

@Deprecated("gunakan socket")
class ClientActivity : AppCompatActivity(), ClientActivityContract {
    override val presenter: ClientPresenter by lazy(LazyThreadSafetyMode.NONE) {
        ClientPresenter(this)
    }
    override val wifiP2pManager: WifiP2pManager? by lazy {
        getSystemService(Context.WIFI_P2P_SERVICE) as WifiP2pManager?
    }
    override val wifiP2pChannel: WifiP2pManager.Channel? by lazy {
        wifiP2pManager?.initialize(applicationContext, mainLooper, null)
    }
    private lateinit var mBroadcastReceiver: BroadcastReceiver
    private val mIntentFilter = IntentFilter().apply {
        addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION)
        addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION)
        addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION)
        addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION)
    }

    private var mCardableList: MutableList<Cardable> = mutableListOf()
    private var mRecyclerViewAdapter: RecyclerView.Adapter<*> = MessageAdapter(mCardableList)
    private lateinit var clientIp: EditText
    private lateinit var clientConnectButton: Button
    private lateinit var peerRecyclerView: RecyclerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_client)

        safeInit {
            mBroadcastReceiver = WifiDirectBroadcastReceiver(
                    wifiP2pManager!!,
                    wifiP2pChannel!!,
                    presenter
            )
        }
        presenter.bindView()
        presenter.setupView()
        presenter.onViewCreated()
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(mBroadcastReceiver, mIntentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(mBroadcastReceiver)
    }

    private fun safeInit(initAction: () -> Unit) {
        try {
            initAction.invoke()
        } catch (e: Exception) {
            presenter.onException(e)
        }
    }

    override fun handleException(e: Exception) {
        val alertDialog: AlertDialog = AlertDialog.Builder(this)
                .setTitle("Caught Exception !")
                .setMessage(e.message)
                .setCancelable(true)
                .setOnCancelListener {  }
                .create()

        alertDialog.show()
    }

    override fun bindView() {
        clientIp = findViewById(R.id.client_ip)
        clientConnectButton = findViewById(R.id.client_connect)
        peerRecyclerView = findViewById(R.id.recycler_view)
    }

    override fun setupView() {
        clientConnectButton.setOnClickListener {
//            presenter.onConnectClicked(clientIp.text, 8888)
        }

        peerRecyclerView.setHasFixedSize(true)
        peerRecyclerView.layoutManager = LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                true)
        peerRecyclerView.adapter = mRecyclerViewAdapter
    }

    override fun discoverpeers() {
        wifiP2pManager?.discoverPeers(wifiP2pChannel, object : WifiP2pManager.ActionListener {
            override fun onSuccess() {

            }

            override fun onFailure(reason: Int) {}
        })
    }

    override fun requestPeers() {
        wifiP2pManager?.requestPeers(wifiP2pChannel) {
            val refreshedPeers = it.deviceList
            mCardableList.clear()

            refreshedPeers.forEachIndexed { index, wifiP2pDevice ->
                mCardableList.add(
                        object : Cardable {
                            override var title: String? = wifiP2pDevice.deviceName
                            override var content: String? = wifiP2pDevice.deviceAddress
                            override var meta: String? = wifiP2pDevice.primaryDeviceType
                        })
            }
            mRecyclerViewAdapter.notifyDataSetChanged()

        }
    }

    override fun connectToPeer(ip: String, port: Int) {
        val config = WifiP2pConfig()
        wifiP2pManager?.connect(wifiP2pChannel, config, object : WifiP2pManager.ActionListener {
            override fun onSuccess() {
                presenter.onConnectedPeer(ip, port)
            }

            override fun onFailure(reason: Int) {
            }

        })
    }

    override fun connectClientToSocket(ip: String, port: Int) {
        val clientAsyncTask = ClientAsyncTask(ip, port, "connect client to socket (POST EX)")
        clientAsyncTask.execute()
    }

    override fun showToast(message: String) {
        super.showToast(message)
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    class WifiDirectBroadcastReceiver(
            private val mWifiP2pManager: WifiP2pManager,
            private val mWifiP2pChannel: WifiP2pManager.Channel,
            private val presenter: WifiP2pContract
    ) : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION -> {
                    presenter.wifiP2pStateChangedAction(intent)
                }
                WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION -> {
                    presenter.wifiP2pPeersChangedAction(intent)
                }
                WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION -> {
                    presenter.wifiP2pConnectionChangedAction(intent)
                }
                WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION -> {
                    presenter.wifiP2pThisDeviceChangedAction(intent)
                }
            }
        }
    }

    inner class ClientAsyncTask internal constructor(
            internal var dstAddress: String,
            internal var dstPort: Int,
            internal var response: String= ""

    ) : AsyncTask<Void, Void, String>() {

        override fun doInBackground(vararg arg0: Void): String {

            var socket: Socket? = null

            try {
                socket = Socket()
                socket.bind(null)
                socket.connect(InetSocketAddress(dstAddress, dstPort), 500)

                val byteArrayOutputStream = ByteArrayOutputStream(
                        1024)
                val buffer = ByteArray(1024)

                val bytesRead: Int
                val inputStream: InputStream = socket.getInputStream()

                /*
             * notice: inputStream.read() will block if no data return
			 */
                bytesRead = inputStream.read(buffer)
                while (bytesRead != -1) {
                    byteArrayOutputStream.write(buffer, 0, bytesRead)
                    response += byteArrayOutputStream.toString("UTF-8")
                }

            } catch (e: UnknownHostException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
                response = "UnknownHostException: " + e.toString()
            } catch (e: IOException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
                response = "IOException: " + e.toString()
            } finally {
                socket?.run {
                    try {
                        close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
            return response
        }

        override fun onPostExecute(result: String) {
            Toast.makeText(this@ClientActivity, response, Toast.LENGTH_SHORT).show()
            super.onPostExecute(result)
        }

    }
}

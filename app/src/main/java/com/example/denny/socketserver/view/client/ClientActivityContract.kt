package com.example.denny.socketserver.view.client

import android.net.wifi.p2p.WifiP2pManager
import com.example.denny.socketserver.view.contract.ViewContract
@Deprecated("gunakan socket")
interface ClientActivityContract: ViewContract {
    val presenter: ClientPresenter
    val wifiP2pManager: WifiP2pManager?
    val wifiP2pChannel: WifiP2pManager.Channel?

    fun handleException(e: Exception)
    fun discoverpeers()
    fun requestPeers()
    fun connectToPeer(ip: String, port: Int)
    fun connectClientToSocket(ip: String, port: Int)
    fun showToast(mesage: String) {}
    fun showSnackBar(message:String) {}

}
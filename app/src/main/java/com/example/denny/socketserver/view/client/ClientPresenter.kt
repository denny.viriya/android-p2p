package com.example.denny.socketserver.view.client

import android.content.Intent
import android.net.wifi.p2p.WifiP2pManager
import android.text.Editable
import com.example.denny.socketserver.view.contract.ViewContract
import com.example.denny.socketserver.view.contract.WifiP2pContract
@Deprecated("gunakan socket")
class ClientPresenter(private var view: ClientActivityContract?) : ViewContract, WifiP2pContract {
    override fun bindView() {
        view?.bindView()
    }

    override fun setupView() {
        view?.setupView()
    }

    fun onException(e: Exception) {
        view?.handleException(e)
    }

    fun onConnectClicked(ip: Editable?, port: Int) {
        if (ip.isNullOrBlank()) {
            return
        }
        val cleanIp = ip!!.trim().toString()
        view?.connectToPeer(cleanIp, port )
    }

    fun onConnectedPeer(ip: String, port: Int) {
        view?.run {
            connectClientToSocket(ip, port)
            showToast("connect client to socket")
        }
    }

    fun onViewCreated() {
        view?.run {
            discoverpeers()
            showToast("discover peers")
        }

    }

    override fun wifiP2pStateChangedAction(intent: Intent) {
        val state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1)
        when (state) {
            WifiP2pManager.WIFI_P2P_STATE_ENABLED -> {

            }

            else -> {

            }
        }
    }

    override fun wifiP2pPeersChangedAction(intent: Intent) {
        view?.run {
            requestPeers()
            showToast("request Peers")
        }
    }

    override fun wifiP2pConnectionChangedAction(intent: Intent) {
    }

    override fun wifiP2pThisDeviceChangedAction(intent: Intent) {
    }
}

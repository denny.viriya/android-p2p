package com.example.denny.socketserver.view.contract

@Deprecated("MVVM pure saja")
interface ViewContract {
    fun bindView()
    fun setupView()
}
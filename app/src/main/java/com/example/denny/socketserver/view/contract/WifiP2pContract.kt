package com.example.denny.socketserver.view.contract

import android.content.Intent

@Deprecated("Gunakan Socket dan ServerSocket")
interface WifiP2pContract {
    fun wifiP2pStateChangedAction(intent: Intent)
    fun wifiP2pPeersChangedAction(intent: Intent)
    fun wifiP2pConnectionChangedAction(intent: Intent)
    fun wifiP2pThisDeviceChangedAction(intent: Intent)
}
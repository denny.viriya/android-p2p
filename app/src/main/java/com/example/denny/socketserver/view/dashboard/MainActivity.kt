package com.example.denny.socketserver.view.dashboard

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.CardView
import com.example.denny.socketserver.R
import com.example.denny.socketserver.view.client.ClientActivity
import com.example.denny.socketserver.view.groupclient.GroupWifiClientActivity
import com.example.denny.socketserver.view.groupserver.GroupWifiServerActivity
import com.example.denny.socketserver.view.server.ServerActivity
import com.example.denny.socketserver.view.socketclient.SocketClientActivity
import com.example.denny.socketserver.view.socketserver.SocketServerActivity
import kotlinx.android.synthetic.main.cardview_standart.view.*

class MainActivity : AppCompatActivity(), MainPresenter.MainActivityContract {

    override val presenter: MainPresenter by lazy(LazyThreadSafetyMode.NONE) {
        MainPresenter(this)
    }
    private lateinit var serverCardView: CardView
    private lateinit var clientCardView: CardView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter.bindView()
        presenter.setupView()
    }

    override fun bindView() {
        serverCardView = findViewById(R.id.card_server)
        clientCardView = findViewById(R.id.card_client)
    }

    override fun setupView() {
        with(serverCardView) {
            title.text = "Server Activity"
            subtitle.text = "Open the server activity and wait for connection"
            setOnClickListener { presenter.onServerCardClicked() }
        }

        with(clientCardView) {
            title.text = "Client Activity"
            subtitle.text = "Open the client activity and search for connection"
            setOnClickListener { presenter.onClientCardClicked() }
        }
    }

    override fun openServerSocketActivity() {
        startActivity(Intent(this, SocketServerActivity::class.java))
    }

    override fun openClientSocketActivity() {
        startActivity(Intent(this, SocketClientActivity::class.java))
    }

}

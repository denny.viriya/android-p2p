package com.example.denny.socketserver.view.dashboard

import com.example.denny.socketserver.view.contract.ViewContract

class MainPresenter(var view: MainActivityContract?) : ViewContract {
    fun onServerCardClicked() {
        view?.openServerSocketActivity()
    }

    fun onClientCardClicked() {
        view?.openClientSocketActivity()
    }

    override fun bindView() {
        view?.bindView()
    }

    override fun setupView() {
        view?.setupView()
    }

    interface MainActivityContract: ViewContract {
        val presenter: MainPresenter

        fun openServerSocketActivity()
        fun openClientSocketActivity()
    }
}
package com.example.denny.socketserver.view.global

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.denny.socketserver.R
import com.example.denny.socketserver.model.Cardable

open class MessageAdapter<T: Cardable>(
        private val cardableList: MutableList<T>
): RecyclerView.Adapter<MessageAdapter.MessageViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.list_standart, parent, false)

        return MessageViewHolder(view)
    }

    override fun getItemCount(): Int {
        return cardableList.size
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        holder.title.text = cardableList[position].title
        holder.subtitle.text = cardableList[position].content
        holder.meta.text = cardableList[position].meta
    }


    class MessageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.title)
        val subtitle: TextView = itemView.findViewById(R.id.subtitle)
        val meta: TextView = itemView.findViewById(R.id.meta)
    }
}
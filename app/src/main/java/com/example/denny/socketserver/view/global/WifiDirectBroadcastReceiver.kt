package com.example.denny.socketserver.view.global

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.wifi.p2p.WifiP2pManager

class WifiDirectBroadcastReceiver(
        private val mInteractor: WifiInteractor
) : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val action: String = intent.action
        when (action) {
            WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION -> {
                // Check to see if Wi-Fi is enabled and notify appropriate activity
                mInteractor.onWifiStateChangedAction(intent)
            }
            WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION -> {
                // Call WifiP2pManager.requestPeers() to get a list of current peers
                mInteractor.onWifiPeersChangedAction(intent)
            }
            WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION -> {
                // Respond to new connection or disconnections
                mInteractor.onWifiConnectionChangedAction(intent)
            }
            WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION -> {
                // Respond to this device's wifi state changing
                mInteractor.onWifiThisDeviceChangedAction(intent)
            }
        }
    }


    interface WifiInteractor {
        fun onWifiStateChangedAction(intent: Intent)
        fun onWifiPeersChangedAction(intent: Intent)
        fun onWifiConnectionChangedAction(intent: Intent)
        fun onWifiThisDeviceChangedAction(intent: Intent)
    }
}
package com.example.denny.socketserver.view.groupclient

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.net.wifi.WpsInfo
import android.net.wifi.p2p.*
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.format.Formatter
import android.util.Log
import android.widget.Toast
import com.example.denny.socketserver.R
import com.example.denny.socketserver.model.Cardable
import com.example.denny.socketserver.model.CardableWifiDevice
import com.example.denny.socketserver.model.Repository
import com.example.denny.socketserver.view.global.MessageAdapter
import com.example.denny.socketserver.view.global.WifiDirectBroadcastReceiver
import com.example.denny.socketserver.view.socketclient.SocketClientActivity
import kotlinx.android.synthetic.main.activity_group_wifi_client.*
import java.io.*
import java.lang.IllegalArgumentException
import java.lang.ref.WeakReference
import java.net.InetSocketAddress
import java.net.Socket

@Deprecated("gunakan socket")
class GroupWifiClientActivity : AppCompatActivity(), WifiDirectBroadcastReceiver.WifiInteractor {

    private val mManager: WifiP2pManager? by lazy(LazyThreadSafetyMode.NONE) {
        getSystemService(Context.WIFI_P2P_SERVICE) as WifiP2pManager?
    }

    private var mChannel: WifiP2pManager.Channel? = null
    private var mReceiver: BroadcastReceiver? = null

    private val mIntentFilter = IntentFilter().apply {
        addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION)
        addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION)
        addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION)
        addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION)
    }

    private var mPeerList: MutableList<CardableWifiDevice> = mutableListOf()
    private var mAdapter: RecyclerView.Adapter<*> = PeerAdapter(mPeerList)

    private lateinit var mBroadcastReceiver: BroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_wifi_client)

        initializeWifiManager()
        initializeListener()
        initializeViewComponent()
    }

    private fun initializeWifiManager() {
        mChannel = mManager?.initialize(this, mainLooper, null)
        mChannel?.also { channel ->
            mReceiver = WifiDirectBroadcastReceiver(this)
        }
    }

    private fun initializeListener() {
        discover_btn.setOnClickListener {
            status_tv.text = getIpAddress()
            discoverPeers()
            SocketClientImpl(WeakReference(this@GroupWifiClientActivity)).execute("192.168.100.10")
        }

        close_btn.setOnClickListener {
            cancelConnect()
        }
    }

    private fun initializeViewComponent() {
        supportActionBar?.title = "Client"

        recycler_view.setHasFixedSize(true)
        recycler_view.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true)
        recycler_view.adapter = mAdapter

        mBroadcastReceiver = WifiDirectBroadcastReceiver(this)
    }

    override fun onResume() {
        super.onResume()
        mReceiver?.also { receiver ->
            registerReceiver(receiver, mIntentFilter)
        }
    }

    override fun onPause() {
        super.onPause()
        mReceiver?.also { receiver ->
            unregisterReceiver(receiver)
        }
    }

    private fun discoverPeers() {
        mManager?.discoverPeers(mChannel, object : WifiP2pManager.ActionListener {
            override fun onSuccess() {

            }

            override fun onFailure(reasonCode: Int) {

            }
        })
    }

    private fun connect(peer: WifiP2pDevice) {
        // Picking the first device found on the network.
        val config = WifiP2pConfig().apply {
            deviceAddress = peer.deviceAddress
            wps.setup = WpsInfo.PBC
        }

        mManager?.connect(mChannel, config, object : WifiP2pManager.ActionListener {

            override fun onSuccess() {
                // WiFiDirectBroadcastReceiver notifies us. Ignore for now.
                showToast("success")
            }

            override fun onFailure(reason: Int) {
                showToast("failure")
            }
        })
    }

    private fun cancelConnect() {
        mManager?.cancelConnect(mChannel, object : WifiP2pManager.ActionListener {
            override fun onSuccess() {
                showToast("cancel connect")
            }

            override fun onFailure(reason: Int) {
                showToast("failed cancel connect")
            }

        })
    }

    private fun requestGroupInfo() {
        mManager?.requestGroupInfo(mChannel) { group ->
            val groupPassword = group.passphrase
        }
    }

    private fun getIpAddress(): String {
        val wm = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        return Formatter.formatIpAddress(wm.connectionInfo.ipAddress)
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onWifiStateChangedAction(intent: Intent) {
        val state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1)
        when (state) {
            WifiP2pManager.WIFI_P2P_STATE_ENABLED -> {
                // Wifi P2P is enabled
            }
            else -> {
                // Wi-Fi P2P is not enabled
            }
        }
    }

    override fun onWifiPeersChangedAction(intent: Intent) {
        mManager?.requestPeers(mChannel) { peers: WifiP2pDeviceList? ->
            // Handle peers list
            mPeerList.clear()
            peers?.deviceList.let {
                mPeerList.addAll(it?.map {device : WifiP2pDevice ->
                    CardableWifiDevice(device)
                } ?: emptyList<CardableWifiDevice>() .toList())
            }
            mAdapter.notifyDataSetChanged()
        }
    }

    override fun onWifiConnectionChangedAction(intent: Intent) {
        val networkInfo: NetworkInfo? = intent
                .getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO) as NetworkInfo

        if (networkInfo?.isConnected == true) {
            mManager?.requestConnectionInfo(mChannel) { info ->

                // InetAddress from WifiP2pInfo struct.
                val groupOwnerAddress: String = info.groupOwnerAddress.hostAddress

                // After the group negotiation, we can determine the group owner
                // (server).
                if (info.groupFormed && info.isGroupOwner) {
                    // Do whatever tasks are specific to the group owner.
                    // One common case is creating a group owner thread and accepting
                    // incoming connections.
                } else if (info.groupFormed) {
                    // The other device acts as the peer (client). In this case,
                    // you'll want to create a peer thread that connects
                    // to the group owner.
                    showToast("Group formed ${info.groupOwnerAddress}")
                }
            }
        }

        showToast(networkInfo?.state?.name ?: "null bang")
    }

    override fun onWifiThisDeviceChangedAction(intent: Intent) {
    }


    inner class PeerAdapter<T: Cardable>(
            private var itemList: MutableList<T>
    ): MessageAdapter<T>(itemList) {

        override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
            super.onBindViewHolder(holder, position)
            holder.itemView.setOnClickListener {
                connect(itemList[position] as WifiP2pDevice)
            }
        }
    }

    companion object {
        class SocketClientImpl(
                private val activity: WeakReference<GroupWifiClientActivity?>
        ): AsyncTask<String, Unit, String?>() {
            var socket: Socket? = null

            override fun doInBackground(vararg params: String?): String? {
                    var buf: ByteArray
                    var len = 0
                    var inputStream: InputStream? = null
                    var outputStream: OutputStream? = null

                    params.forEach { remoteIpAddress ->
                        try {
//                            val localIpAddress = activity.get()?.getIpAddress()

                            socket = Socket()
                            socket!!.bind(null)
                            socket!!.connect(
                                    InetSocketAddress(remoteIpAddress, Repository.PORT),
                                    60000
                            )

                            inputStream = socket!!.getInputStream()
                            outputStream = socket!!.getOutputStream()
                            outputStream!!.bufferedWriter(Charsets.UTF_8).write("Hello from client")

                            buf = inputStream!!.readBytes()

                            while (inputStream!!.read(buf).also { len = it } != -1) {
//                            outputStream!!.write(buf, 0, len)
                                Log.d("input stream read", buf.toString())
                            }

                        } catch (e: IOException) {
                            e.printStackTrace()
                        } catch (e: SecurityException) {
                            e.printStackTrace()
                        } catch (e: IllegalArgumentException) {
                            e.printStackTrace()
                        } finally {
//                        inputStream?.close()
//                        outputStream?.close()
//                        socket?.takeIf { it.isConnected }?.apply { close() }
                        }
                    }

                    return inputStream?.bufferedReader(Charsets.UTF_8)?.use { it.readText() }
            }

            override fun onPostExecute(result: String?) {
                super.onPostExecute(result)
                showToast(result ?: "result is null")

//                socket?.takeIf { it.isConnected }?.apply { close() }
            }

            private fun showToast(message: String) {
                activity.get()?.run {
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
                }
            }

        }
    }
}

package com.example.denny.socketserver.view.groupserver


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.net.wifi.WpsInfo
import android.net.wifi.p2p.WifiP2pConfig
import android.net.wifi.p2p.WifiP2pDevice
import android.net.wifi.p2p.WifiP2pDeviceList
import android.net.wifi.p2p.WifiP2pManager
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.format.Formatter
import android.widget.Toast
import com.example.denny.socketserver.R
import com.example.denny.socketserver.model.Cardable
import com.example.denny.socketserver.model.Repository
import com.example.denny.socketserver.view.global.MessageAdapter
import com.example.denny.socketserver.view.global.WifiDirectBroadcastReceiver
import com.example.denny.socketserver.view.socketserver.SocketServerActivity
import kotlinx.android.synthetic.main.activity_group_wifi_server.*
import java.io.*
import java.lang.IllegalArgumentException
import java.lang.ref.WeakReference
import java.net.ServerSocket
import java.net.Socket

@Deprecated("gunakan socket")
class GroupWifiServerActivity : AppCompatActivity(), WifiDirectBroadcastReceiver.WifiInteractor {

    private val mManager: WifiP2pManager? by lazy(LazyThreadSafetyMode.NONE) {
        getSystemService(Context.WIFI_P2P_SERVICE) as WifiP2pManager?
    }

    private var mChannel: WifiP2pManager.Channel? = null
    private var mReceiver: BroadcastReceiver? = null

    private val mIntentFilter = IntentFilter().apply {
        addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION)
        addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION)
        addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION)
        addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION)
    }

    private var mPeerList: MutableList<Cardable> = mutableListOf()
    private var mAdapter: RecyclerView.Adapter<*> = MessageAdapter(mPeerList)

    private lateinit var mBroadcastReceiver: BroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_wifi_server)

        initializeWifiManager()
        initializeListener()
        initializeViewComponent()
    }

    private fun initializeWifiManager() {
        mChannel = mManager?.initialize(this, mainLooper, null)
        mChannel?.also { channel ->
            mReceiver = WifiDirectBroadcastReceiver(this)
        }
    }

    private fun initializeListener() {
        discover_btn.setOnClickListener {
            status_tv.text = getIpAddress()
            discoverPeers()
            createGroup()
        }

        close_btn.setOnClickListener {
            cancelConnect()
        }
    }

    private fun initializeViewComponent() {
        supportActionBar?.title = "Server"

        recycler_view.setHasFixedSize(true)
        recycler_view.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true)
        recycler_view.adapter = mAdapter

        mBroadcastReceiver = WifiDirectBroadcastReceiver(this)
    }

    override fun onResume() {
        super.onResume()
        mReceiver?.also { receiver ->
            registerReceiver(receiver, mIntentFilter)
        }
    }

    override fun onPause() {
        super.onPause()
        mReceiver?.also { receiver ->
            unregisterReceiver(receiver)
        }
    }

    private fun discoverPeers() {
        mManager?.discoverPeers(mChannel, object : WifiP2pManager.ActionListener {
            override fun onSuccess() {

            }

            override fun onFailure(reasonCode: Int) {

            }
        })
    }

    private fun connect(peers: MutableList<WifiP2pDevice>) {
        // Picking the first device found on the network.
        val device = peers[0]

        val config = WifiP2pConfig().apply {
            deviceAddress = device.deviceAddress
            wps.setup = WpsInfo.PBC
        }

        mManager?.connect(mChannel, config, object : WifiP2pManager.ActionListener {

            override fun onSuccess() {
                // WiFiDirectBroadcastReceiver notifies us. Ignore for now.
            }

            override fun onFailure(reason: Int) {

            }
        })
    }

    private fun cancelConnect() {
        mManager?.cancelConnect(mChannel, object : WifiP2pManager.ActionListener {
            override fun onSuccess() {
                showToast("cancel connect")
            }

            override fun onFailure(reason: Int) {
                showToast("failed cancel connect")
            }

        })
    }

    private fun requestGroupInfo() {
        mManager?.requestGroupInfo(mChannel) { group ->
            val groupPassword = group.passphrase
        }
    }

    private fun createGroup() {
        mManager?.createGroup(mChannel, object : WifiP2pManager.ActionListener {
            override fun onSuccess() {
                // Device is ready to accept incoming connections from peers.
            }

            override fun onFailure(reason: Int) {

            }
        })
    }

    private fun getIpAddress(): String {
        val wm = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        return Formatter.formatIpAddress(wm.connectionInfo.ipAddress)
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onWifiStateChangedAction(intent: Intent) {
        val state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1)
        when (state) {
            WifiP2pManager.WIFI_P2P_STATE_ENABLED -> {
                // Wifi P2P is enabled
            }
            else -> {
                // Wi-Fi P2P is not enabled
            }
        }
    }

    override fun onWifiPeersChangedAction(intent: Intent) {
        mManager?.requestPeers(mChannel) { peers: WifiP2pDeviceList? ->
            // Handle peers list
            mPeerList.clear()
            peers?.deviceList.let {
                mPeerList.addAll(it?.map {device : WifiP2pDevice ->
                    object : Cardable {
                        override var title: String? = device.deviceName
                        override var content: String? = device.deviceAddress
                        override var meta: String? = device.primaryDeviceType

                    }
                } ?: emptyList<Cardable>() .toList())
            }

            mAdapter.notifyDataSetChanged()
        }
    }

    override fun onWifiConnectionChangedAction(intent: Intent) {
        val networkInfo: NetworkInfo? = intent
                .getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO) as NetworkInfo

        if (networkInfo?.isConnected == true) {
            mManager?.requestConnectionInfo(mChannel) { info ->
                // InetAddress from WifiP2pInfo struct.
                val groupOwnerAddress: String = info.groupOwnerAddress.hostAddress

                // After the group negotiation, we can determine the group owner
                // (server).
                if (info.groupFormed && info.isGroupOwner) {
                    // Do whatever tasks are specific to the group owner.
                    // One common case is creating a group owner thread and accepting
                    // incoming connections.
                    showToast("created group ${info.groupOwnerAddress} isOwner -> ${info.isGroupOwner}")
                    SocketServerImpl(WeakReference(this@GroupWifiServerActivity)).execute()
                } else if (info.groupFormed) {
                    // The other device acts as the peer (client). In this case,
                    // you'll want to create a peer thread that connects
                    // to the group owner.
                }
            }
        }

        showToast(networkInfo?.state?.name ?: "null bang")
    }

    override fun onWifiThisDeviceChangedAction(intent: Intent) {
    }


    companion object {
        class SocketServerImpl(
                private val activity: WeakReference<GroupWifiServerActivity?>
        ): AsyncTask<Unit, Unit, String?>() {
            var serverSocket: ServerSocket? = null

            override fun doInBackground(vararg params: Unit): String? {
                val socket: Socket?
                var inputStream: InputStream? = null
                var outputStream: OutputStream? = null

                try {
                    serverSocket = ServerSocket(Repository.PORT)
                    socket = serverSocket!!.accept()

                    inputStream = socket.getInputStream()
                    outputStream = socket.getOutputStream()

                    outputStream.bufferedWriter(Charsets.UTF_8).write("ok from server")


                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: SecurityException) {
                    e.printStackTrace()
                } catch (e: IllegalArgumentException) {
                    e.printStackTrace()
                } finally {
//                    serverSocket?.run {
//                        try {
//                            serverSocket.close()
//                        } catch (e: IOException) {
//                            e.printStackTrace()
//                        }
//                    }
                }

                return inputStream?.bufferedReader(Charsets.UTF_8)?.use { it.readText() }
            }

            override fun onPostExecute(result: String?) {
                super.onPostExecute(result)
                showToast(result ?: "result is null")
                serverSocket?.run {
                    try {
                        serverSocket?.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }

            private fun showToast(message: String) {
                activity.get()?.run {
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
                }
            }

        }
    }
}

package com.example.denny.socketserver.view.server

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.wifi.WifiManager
import android.net.wifi.p2p.WifiP2pConfig
import android.net.wifi.p2p.WifiP2pManager
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.format.Formatter
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.denny.socketserver.R
import com.example.denny.socketserver.model.Cardable
import com.example.denny.socketserver.view.contract.WifiP2pContract
import com.example.denny.socketserver.view.global.MessageAdapter
import kotlinx.android.synthetic.main.activity_server.*
import java.io.IOException
import java.lang.IllegalArgumentException
import java.net.ServerSocket

@Deprecated("gunakan socket")
class ServerActivity : AppCompatActivity(), ServerActivityContract {
    override val presenter: ServerPresenter by lazy {
        ServerPresenter(this)
    }
    override val wifiP2pManager: WifiP2pManager? by lazy {
        getSystemService(Context.WIFI_P2P_SERVICE) as WifiP2pManager?
    }
    override val wifiP2pChannel: WifiP2pManager.Channel? by lazy {
        wifiP2pManager?.initialize(applicationContext, mainLooper, null)
    }
    private lateinit var mBroadcastReceiver: BroadcastReceiver
    private val mIntentFilter = IntentFilter().apply {
        addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION)
        addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION)
        addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION)
        addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION)
    }

    private lateinit var mBroadcastButton: Button
    private lateinit var mStatusTextView: TextView
    private lateinit var mRecyclerView: RecyclerView
    private var mCardableList: MutableList<Cardable> = mutableListOf()
    private var mRecyclerViewAdapter: RecyclerView.Adapter<*> = MessageAdapter(mCardableList)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_server)

        safeInit {
            mBroadcastReceiver = WifiDirectBroadcastReceiver(
                    wifiP2pManager!!,
                    wifiP2pChannel!!,
                    presenter
            )
        }
        presenter.onViewCreated()
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(mBroadcastReceiver, mIntentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(mBroadcastReceiver)
    }

    override fun bindView() {
        mBroadcastButton = findViewById(R.id.broadcast_button)
        mStatusTextView = findViewById(R.id.status_textview)
        mRecyclerView = findViewById(R.id.recyclerview)
    }

    override fun setupView() {
        mRecyclerView.setHasFixedSize(false)
        mRecyclerView.layoutManager = LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                true
        )
        mRecyclerView.adapter = mRecyclerViewAdapter
        mStatusTextView.text = "not determined"
        mBroadcastButton.setOnClickListener { presenter.onBroadcastButtonClicked() }
    }

    override fun getIpAddress(): String {
        val wm = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val ip = Formatter.formatIpAddress(wm.connectionInfo.ipAddress)
        Toast.makeText(this@ServerActivity, ip, Toast.LENGTH_SHORT).show()
        return ip
    }

    override fun openServerSocket(ip: String, port: Int) {
        val serverAsync = ServerAsyncTask(ip, port)
        serverAsync.execute()
    }

    override fun setServerConnectionStatus(status: String) {
        status_textview.text = status
    }

    override fun discoverPeers() {
        wifiP2pManager?.discoverPeers(wifiP2pChannel, object : WifiP2pManager.ActionListener {
            override fun onSuccess() {
                presenter.onDiscovered(true)
            }

            override fun onFailure(reason: Int) {
                presenter.onDiscovered(false)
            }
        })
    }

    override fun requestPeers() {
        wifiP2pManager?.requestPeers(wifiP2pChannel) {
            presenter.onRequestPeersRefreshed(it)
        }
    }

    override fun reloadWifiRecyclerView(deviceList: MutableList<Cardable>) {
        mCardableList.clear()

        deviceList.forEachIndexed { index, wifiP2pDevice ->
            mCardableList.add(
                    object : Cardable {
                        override var title: String? = wifiP2pDevice.title
                        override var content: String? = wifiP2pDevice.content
                        override var meta: String? = wifiP2pDevice.meta
                    })
        }
        mRecyclerViewAdapter.notifyDataSetChanged()
    }

    override fun connectToPeer() {
        val config = WifiP2pConfig()
        wifiP2pManager?.connect(wifiP2pChannel, config, object : WifiP2pManager.ActionListener {
            override fun onSuccess() {
            }

            override fun onFailure(reason: Int) {
            }

        })
    }

    override fun showToast(message: String) {
        super.showToast(message)
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun handleExceptionWithAlertDialog(e: Exception) {
        val alertDialog: AlertDialog = AlertDialog.Builder(this)
                .setTitle("Caught Exception !")
                .setMessage(e.toString())
                .setCancelable(true)
                .setOnCancelListener { }
                .create()

        alertDialog.show()
    }

    private fun safeInit(initAction: () -> Unit) {
        try {
            initAction.invoke()
        } catch (e: Exception) {
            presenter.onException(e)
        }
    }

    class WifiDirectBroadcastReceiver(
            private val mWifiP2pManager: WifiP2pManager,
            private val mWifiP2pChannel: WifiP2pManager.Channel,
            private val presenter: WifiP2pContract
    ) : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION -> {
                    presenter.wifiP2pStateChangedAction(intent)
                }
                WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION -> {
                    presenter.wifiP2pPeersChangedAction(intent)
                }
                WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION -> {
                    presenter.wifiP2pConnectionChangedAction(intent)
                }
                WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION -> {
                    presenter.wifiP2pThisDeviceChangedAction(intent)
                }
            }
        }
    }

    inner class ServerAsyncTask internal constructor(
            internal var dstAddress: String,
            internal var dstPort: Int
    ): AsyncTask<Unit, Unit, String>() {
        override fun doInBackground(vararg params: Unit?): String {
            var serverSocket: ServerSocket? = null

            try {
                serverSocket = ServerSocket(dstPort)
                val client = serverSocket.accept()
            } catch (e: IOException) {

            } catch (e: SecurityException) {

            } catch (e: IllegalArgumentException) {

            } finally {
                serverSocket?.run {
                    try {
                        serverSocket.close()
                    } catch (e: IOException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }
                }
            }

            return "Something from server ASYNC"
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            Toast.makeText(this@ServerActivity, "Server done", Toast.LENGTH_SHORT).show()
        }

    }
}

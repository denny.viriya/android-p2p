package com.example.denny.socketserver.view.server

import android.net.wifi.p2p.WifiP2pManager
import com.example.denny.socketserver.model.Cardable
import com.example.denny.socketserver.view.contract.ViewContract

@Deprecated("gunakan socket")
interface ServerActivityContract: ViewContract {
    val presenter: ServerPresenter
    val wifiP2pManager: WifiP2pManager?
    val wifiP2pChannel: WifiP2pManager.Channel?

    fun handleExceptionWithAlertDialog(e: Exception)
    fun discoverPeers()
    fun requestPeers()
    fun connectToPeer()
    fun openServerSocket(ip:String, port:Int)
    fun getIpAddress(): String
    fun setServerConnectionStatus(status: String)
    fun reloadWifiRecyclerView(deviceList: MutableList<Cardable>)
    fun showToast(message: String) {}
    fun showSnackBar() {}
}
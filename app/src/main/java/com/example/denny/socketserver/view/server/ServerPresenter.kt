package com.example.denny.socketserver.view.server

import android.content.Intent
import android.net.wifi.p2p.WifiP2pDevice
import android.net.wifi.p2p.WifiP2pDeviceList
import com.example.denny.socketserver.model.Cardable
import com.example.denny.socketserver.view.contract.ViewContract
import com.example.denny.socketserver.view.contract.WifiP2pContract


@Deprecated("gunakan socket")
class ServerPresenter(private var view: ServerActivityContract?): ViewContract, WifiP2pContract {

    fun onViewCreated() {
        view?.run {
            bindView()
            setupView()
        }
    }

    fun connectToPeers() {
        view?.connectToPeer()
    }

    override fun bindView() {
        view?.bindView()
    }

    override fun setupView() {
        view?.setupView()
    }

    fun onException(e: Exception) {
        e.printStackTrace()
        view?.handleExceptionWithAlertDialog(e)
    }

    fun onBroadcastButtonClicked() {
//        val ip: String? = view?.getIpAddress()
//        val port = 8888
//        view?.openServerSocket(ip ?: "", port)
//        view?.setServerConnectionStatus("$ip:$port")
        view?.run {
            discoverPeers()
            showToast("discovering peers")
        }
    }

    fun onDiscovered(success: Boolean) {
        if (success) {
            view?.requestPeers()
            view?.showToast("requesting peers ...")
        } else {
            view?.showToast("Failed discovering peers")
        }
    }

    fun onRequestPeersRefreshed(wifiP2pDeviceList: WifiP2pDeviceList) {
        view?.reloadWifiRecyclerView(
                wifiP2pDeviceList.deviceList.map { device: WifiP2pDevice ->
                    object : Cardable {
                        override var title: String? = device.deviceName
                        override var content: String? = device.deviceAddress
                        override var meta: String? = device.primaryDeviceType
                    }
                }.toMutableList()
        )
    }

    override fun wifiP2pStateChangedAction(intent: Intent) {  }

    override fun wifiP2pPeersChangedAction(intent: Intent) {
        view?.requestPeers()
    }

    override fun wifiP2pConnectionChangedAction(intent: Intent) {  }

    override fun wifiP2pThisDeviceChangedAction(intent: Intent) {  }
}
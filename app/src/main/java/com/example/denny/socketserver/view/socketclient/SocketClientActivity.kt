package com.example.denny.socketserver.view.socketclient

import android.content.Context
import android.net.wifi.WifiManager
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.format.Formatter
import android.view.View
import android.widget.Toast
import com.example.denny.socketserver.R
import com.example.denny.socketserver.model.Repository
import kotlinx.android.synthetic.main.activity_socket_client.*
import java.io.*
import java.lang.IllegalArgumentException
import java.lang.ref.WeakReference
import java.net.Socket

class SocketClientActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_socket_client)

        setupViewComponent()
        initializeButtonListener()
    }

    private fun initializeButtonListener() {
        client_connect_button.setOnClickListener(onClientConnect)
    }

    private val onClientConnect = View.OnClickListener {
        val ip = getTargetIp()
        showToast("Running Client...")
        runClient(ip)
    }

    private fun setupViewComponent() {
        val ip = getIpAddress()
        val prefixIp = createPrefixIp(ip)
        setStatusText(prefixIp)
    }

    private fun createPrefixIp(ipAddress: String): String {
        val ipBuilder = StringBuilder()
        val ipChunkList = ipAddress.split(".")
        for (i in 0 until ipChunkList.lastIndex) {
            ipBuilder.append(ipChunkList[i])
            ipBuilder.append(".")
        }
        return ipBuilder.toString()
    }

    private fun getTargetIp(): String {
        return client_ip_address.text.toString()
    }

    private fun setStatusText(initialIp: String) {
        client_ip_address.setText(initialIp)
    }

    private fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    private fun showAlertDialog(title: String, message: String) {
        AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .create()
                .show()
    }

    private fun getIpAddress(): String {
        val wm = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        return Formatter.formatIpAddress(wm.connectionInfo.ipAddress)
    }

    private fun runClient(ipAddress: String) {
        val socketClient = SocketClientTask(WeakReference(this))
        socketClient.execute(ipAddress)
    }

    companion object {
        class SocketClientTask(
                private val activity: WeakReference<SocketClientActivity?>
        ) : AsyncTask<String, Unit, String?>() {
            override fun doInBackground(vararg params: String?): String? {
                var socket: Socket? = null
                var dataOutputStream: DataOutputStream? = null
                var dataInputStream: DataInputStream? = null
                var inputText = ""

                try {
                    socket = Socket(params.first(), Repository.PORT)
                    dataOutputStream = DataOutputStream(socket.getOutputStream())
                    dataInputStream = DataInputStream(socket.getInputStream())
                    dataOutputStream.writeUTF("Write to data output")
                    inputText = dataInputStream.readUTF()

                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: SecurityException) {
                    e.printStackTrace()
                } catch (e: IllegalArgumentException) {
                    e.printStackTrace()
                } finally {
                    if (socket != null) {
                        try {
                            socket.close();
                        } catch (e: IOException) {
                            e.printStackTrace();
                            throw (e)
                        }
                    }
                }

                if (dataOutputStream != null) {
                    try {
                        dataOutputStream.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }

                if (dataInputStream != null) {
                    try {
                        dataInputStream.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }

                return inputText
            }

            override fun onPostExecute(result: String?) {
                super.onPostExecute(result)
                showToast(result ?: "result is null")
            }

            private fun showToast(message: String) {
                activity.get()?.run {
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
                }
            }

        }
    }
}

package com.example.denny.socketserver.view.socketclient

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent

@Deprecated("over-engineer!")
class SocketClientPresenter(
        private val view: SocketClientInteractor?
): LifecycleObserver {

    fun handleConnectButtonClick() {
        val targetIp = view?.getTargetIp() ?: ""
        view?.runClient(targetIp.trim())
        view?.showToast("run client")
    }

    fun prepareView() {
        val ipAddress: String = view?.getIpAddress() ?: ""
        val initialIp = createPrefixIp(ipAddress)
        view?.setStatusText(initialIp)
    }

    private fun createPrefixIp(ipAddress: String): String {
        val ipBuilder = StringBuilder()
        val ipChunkList = ipAddress.split(".")
        for (i in 0 until ipChunkList.lastIndex) {
            ipBuilder.append(ipChunkList[i])
            ipBuilder.append(".")
        }
        return ipBuilder.toString()
    }

    @Deprecated("over-engineer!")
    interface SocketClientInteractor: LifecycleOwner {
        fun showToast(text: String) {}
        fun showAlertDialog(title: String, message: String) {}
        fun runClient(ipAddress: String) {}
        fun getIpAddress(): String
        fun setStatusText(initialIp: String) {}
        fun getTargetIp(): String
    }
}
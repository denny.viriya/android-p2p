package com.example.denny.socketserver.view.socketserver

import android.content.Context
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.Toast
import com.example.denny.socketserver.R
import com.example.denny.socketserver.model.Repository
import kotlinx.android.synthetic.main.activity_socket_server.*
import java.lang.ref.WeakReference
import java.net.*
import android.net.wifi.WifiManager
import android.text.format.Formatter
import android.view.View
import java.io.*


class SocketServerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_socket_server)

        initializeButtonListener()
    }

    private fun initializeButtonListener() {
        server_open_socket_button.setOnClickListener(onServerOpenSocketButtonClicked)
    }

    private val onServerOpenSocketButtonClicked = View.OnClickListener {
        val ip = getIpAddress()
        runServer()
        setStatusText(ip)
    }

    fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    private fun showAlertDialog(title: String, message: String) {
        AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .create()
                .show()
    }

    private fun setStatusText(text: String) {
        server_state_textview.text = text
    }

    private fun getIpAddress(): String {
        val wm = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        return Formatter.formatIpAddress(wm.connectionInfo.ipAddress)
    }

    private fun runServer() {
        val socketServer = SocketServerTask(WeakReference(this))
        socketServer.execute()
    }

    companion object {
        class SocketServerTask(
                private val activity: WeakReference<SocketServerActivity?>
        ) : AsyncTask<Unit, Unit, String?>() {
            override fun doInBackground(vararg params: Unit?): String? {
                var serverSocket: ServerSocket? = null
                var socket: Socket? = null
                var dataOutputStream: DataOutputStream? = null
                var dataInputStream: DataInputStream? = null
                var inputText = ""

                try {
                    serverSocket = ServerSocket(Repository.PORT)
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                while (true) {
                    try {
                        socket = serverSocket!!.accept()
                        dataInputStream = DataInputStream(socket.getInputStream())
                        dataOutputStream = DataOutputStream(socket.getOutputStream())
                        dataOutputStream.writeUTF("ip: " + socket.inetAddress)
                        dataOutputStream.writeUTF("message: " + dataInputStream.readUTF())
                        dataOutputStream.writeUTF("Hello!")
                        inputText = dataInputStream.readUTF()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } finally {
                        if (socket != null) {
                            try {
                                socket.close()
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }

                        }

                        if (dataInputStream != null) {
                            try {
                                dataInputStream.close()
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }

                        }

                        if (dataOutputStream != null) {
                            try {
                                dataOutputStream.close()
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }

                        }

                        return inputText
                    }
                }
            }

            override fun onPostExecute(result: String?) {
                super.onPostExecute(result)
                showToast(result ?: "result is null")
            }

            private fun showToast(message: String) {
                activity.get()?.run {
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
                }
            }

        }
    }
}

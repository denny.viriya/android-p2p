package com.example.denny.socketserver.view.socketserver

@Deprecated("over-engineer!")
class SocketServerPresenter(
        private val view: SocketServerInteractor?
) {
    fun handleOpenSocketButtonClick() {
        view?.runServer()
        view?.setStatusText(view.getIpAddress())
    }

    @Deprecated("over-engineer!")
    interface SocketServerInteractor {

        fun showToast(text: String) {}
        fun showAlertDialog(title: String, message: String) {}
        fun runServer() {}
        fun setStatusText(text: String) {}
        fun getIpAddress(): String
    }
}